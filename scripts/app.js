/* /////////////////////////// */
/* Custom jQuery to render data from API from Open weather */
/* Author: Neil D Ferguson */
/* Date: 13 February 2017  */
/* /////////////////////////// */


$(document).ready(function() {

/* Configuration settings */
  var location     = ( getParameterByName('location')!=null && getParameterByName('location')!="" ) ? getParameterByName('location') : "Edinburgh";
  var noOfDays     = 5;
  var apiKey       = '279b4be6d54c8bf6ea9b12275a567156';
  var apiBaseUrl   = 'http://api.openweathermap.org/data/2.5/';
  var apiUrl       = apiBaseUrl + 'forecast/daily?q=' + location + '&cnt=' + noOfDays + '&appid=' + apiKey;
  var iconBaseUrl  = 'http://openweathermap.org/img/w/';
  var tempKelvin    = 273.15;

/* Call the main ajax function */
  getOpenWeatherData(apiUrl);

/* Main ajax call to Open Weather */
  function getOpenWeatherData(apiUrl){
    $.getJSON(apiUrl,
      function(result){ })
        .done(function(result) {
          getWeatherForecast(result);
        })
        .fail(function(result) {
          logError(result);
        })
        .always(function() {
    });
  }

/* Log any errors -TO DO */
  function logError(result){
    console.log(results);
  }

/* Grab the querystring data */
  function getParameterByName(name, url) {
      if (!url) {
        url = window.location.href;
      }
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

/* Use jQuery Template plugin to replace variables in templates */
  function getWeatherForecast(result){
    renderHeader("#dashHeader", result);
    renderBody("#dashMain",result);
  }

 /* Render the main page header data */
  function renderHeader(div, result){
    $(div).loadTemplate("/templates/partials/headerPanel.html",
      {
        location    : result.city.name,
        country     : result.city.country,
        population  : result.city.population,
        lat         : result.city.coord.lat,
        lon         : result.city.coord.lon
      },
      {
        append: true
      }
    );
  }

/* Render the main body panels data*/
  function renderBody(div, result){
    $(result.list).each(function(index,data)
      {
      var dateTimeToday = new Date(data.dt*1000);

      $(div).loadTemplate("/templates/partials/bodyPanel.html",
        {
          iconUrl       : iconBaseUrl + data.weather[0].icon + '.png',
          description   : data.weather[0].description,
          dateTimeToday : moment(dateTimeToday).format('dddd, MMMM Do, YYYY'),
          tempDay       : (data.temp.day - tempKelvin).toFixed(2),
          tempMin       : (data.temp.min - tempKelvin).toFixed(2),
          tempMax       : (data.temp.max - tempKelvin).toFixed(2),
          tempNight     : (data.temp.night - tempKelvin).toFixed(2),
          tempEve       : (data.temp.eve - tempKelvin).toFixed(2),
          tempMorn      : (data.temp.morn - tempKelvin).toFixed(2),
          pressure      : data.pressure,
          humidity      : data.humidity
        },
        {
          append: true
        }
      );
    }); // end for each
  }
}); // end doc ready
