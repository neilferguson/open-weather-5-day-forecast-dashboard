## Synopsis

A single page web application that connects to Open Weather (https://openweathermap.org/forecast5), retrieves the data from the API and presents it in a dashboard style format. Users can change the weather location, otherwise it will default to Edinburgh.

## Code Example

The application uses:
- HTML
- CSS
- jQuery
- jQuery Template plugin (https://github.com/codepb/jquery-template)
- jQuery Moment plugin (https://momentjs.com/)

The templating is used to separate the Javascript from the HTML .e.g.

```
/* Render the main page header data */
 function renderHeader(div, result){
   $(div).loadTemplate("/templates/partials/headerPanel.html",
     {
       location    : result.city.name,
       country     : result.city.country,
       population  : result.city.population,
       lat         : result.city.coord.lat,
       lon         : result.city.coord.lon
     },
     {
       append: true
     }
   );
 }
```

## Installation

Code can be cloned from Bitbucket (<URL> https://neilferguson@bitbucket.org/neilferguson/open-weather-5-day-forecast-dashboard.git).
To clone this application first use git init, then  git clone <URL>. There are no special dependencies for this application due to it's very simple nature. You can view by running # php -S "localhost:1234" and hen viewing in your browser at http://localhost:1234

## API Reference

Full documentation can be found at https://openweathermap.org/forecast5 for the Open Weather API

## Tests

There are currently no unit tests setup

## Demo

A demo can be found here http://openweather.esy.es

## To Do

- [ ] Consider converting to a jQuery extension
- [ ] Abstract the code to allow for unit testing, e.g. https://qunitjs.com/
- [ ] Add dependancy management using composer, bower or NPM
- [ ] Add more features to allow graphing/charting of data over time
- [ ] Setup caching to reduce API calls to service
- [ ] Introduce SASS and Grunt if customising CSS
